﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TradingSystem
{
    public class Traider : MonoBehaviour
    {
        [SerializeField] private List<int> _itemsId = new List<int>();
        [SerializeField] private ItemBase _itemBase;
        [SerializeField] private Transform _traiderSpawnPanel;
        [SerializeField] private ItemSlot _itemSlotPrefab;
        [SerializeField] private Text _itemDescriptText;
        [SerializeField] private int _itemSlotCount;
        public GameObject traiderInventoryPanel;
        public ItemType traiderType;
        private List<Item> _traiderInventory = new List<Item>();
        private ItemSlot[] _itemSlots;

        void Start()
        {
            TradingManager.Instance.traiderContainer.Add(gameObject, this);

            _itemSlots = new ItemSlot[_itemSlotCount];

            for (int i = 0; i < _itemSlotCount; i++)
            {
                _itemSlots[i] = Instantiate(_itemSlotPrefab, _traiderSpawnPanel);
            }

            for (int i = 0; i < _itemsId.Count; i++)
                _traiderInventory.Add(_itemBase.TakeItemFromBase(_itemsId[i]));

            UpdateTraider();
        }

        public void UpdateTraider()
        {
            foreach (var itemSlot in _itemSlots)
            {
                itemSlot.ActiveItemSlot(null, true);
            }

            for (int i = 0; i < _itemsId.Count; i++)
            {
                if (i < _itemSlots.Length)
                    _itemSlots[i].ActiveItemSlot(_traiderInventory[i], true);
            }
        }

        public void AddItem(int id)
        {
            _traiderInventory.Add(_itemBase.TakeItemFromBase(id));
            _itemsId.Add(id);
        }

        public void RemoveItem(Item item)
        {
            _itemBase.RemoveItemFromList(_traiderInventory, item);
            _itemsId.Remove(item.id);
        }

        public void ViewItemDescription(ItemSlot itemSlot)
        {
            _itemDescriptText.text = "Name: " + itemSlot.item.itemName + ", cost: " + itemSlot.item.cost;
        }
    }
}
