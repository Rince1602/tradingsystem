﻿using UnityEngine;

namespace TradingSystem
{
    public class Player : MonoBehaviour
    {
        private float _speed = 7.0f;

        void Update()
        {
            Movement();
        }

        private void Movement()
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
            transform.Translate(movement * _speed * Time.deltaTime);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Traider")
                TradingManager.Instance.playerInventory.TraiderNear(true, other);
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag == "Traider")
                TradingManager.Instance.playerInventory.TraiderNear(false);
        }
    }
}
