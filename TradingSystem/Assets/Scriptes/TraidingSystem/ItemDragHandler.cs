﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TradingSystem
{
    public class ItemDragHandler : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
    {
        public Transform originalParent;
        private Transform _canvas;

        private void Start()
        {
            _canvas = GameObject.Find("OfferCanvas").transform;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (transform.parent.parent.name == "TraiderPanel")
                TradingManager.Instance.playerInventory.traider.ViewItemDescription(
                    gameObject.transform.parent.GetComponent<ItemSlot>());
            else if (transform.parent.parent.name == "PlayerPanel")
                TradingManager.Instance.playerInventory.ViewItemDescription(gameObject.transform.parent
                    .GetComponent<ItemSlot>());
            originalParent = transform.parent;
            transform.SetParent(_canvas);
            GetComponent<CanvasGroup>().blocksRaycasts = false;
            GetComponent<Image>().raycastTarget = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            transform.position = Input.mousePosition;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            transform.SetParent(originalParent);
            GetComponent<CanvasGroup>().blocksRaycasts = true;
            GetComponent<Image>().raycastTarget = true;
        }
    }
}
