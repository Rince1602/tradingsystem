﻿using System.Collections.Generic;
using UnityEngine;

namespace TradingSystem
{
    [CreateAssetMenu(fileName = "ItemBase", menuName = "Items")]
    public class ItemBase : ScriptableObject
    {
        [SerializeField] private List<Item> _items = new List<Item>();

        public Item TakeItemFromBase(int itemId)
        {
            var tempItem = new Item();
            foreach (Item item in _items)
            {
                if (item.id == itemId)
                    tempItem = item;
            }

            return tempItem;
        }

        public List<Item> RemoveItemFromList(List<Item> itemList, Item tempItem)
        {
            foreach (Item item in itemList)
            {
                if (item.id == tempItem.id)
                {
                    itemList.Remove(item);
                    break;
                }
            }
            return itemList;
        }
    }
}
