﻿using UnityEngine;
using UnityEngine.UI;

namespace TradingSystem
{
    public class ItemSlot : MonoBehaviour
    {
        [SerializeField] private Image _itemIcon;
        [SerializeField] private Image _slotImage;
        public Item item;

        public void ActiveItemSlot(Item item, bool isSell = true)
        {
            
            this.item = item;
            if (item == null)
            {
                _itemIcon.sprite = null;
                _itemIcon.gameObject.SetActive(false);
            }
            else
            {
                if (!_itemIcon.gameObject.activeSelf)
                    _itemIcon.gameObject.SetActive(true);
                _itemIcon.sprite = item.itemImage;
                if (isSell)
                    _itemIcon.color = Color.white;
                else
                    _itemIcon.color = Color.red;
                ;
            }
        }
    }
}


