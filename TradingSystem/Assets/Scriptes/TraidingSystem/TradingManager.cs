﻿using System.Collections.Generic;
using UnityEngine;

namespace TradingSystem
{
    public class TradingManager : MonoBehaviour
    {
        private static TradingManager _instance;
        public static TradingManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    GameObject go = new GameObject("TradingManager");
                    go.AddComponent<TradingManager>();
                }

                return _instance;
            }
        }
        public PlayerInventory playerInventory;
        public Dictionary<GameObject, Traider> traiderContainer;
        public OfferManager offerManager;
        public Money money;
        public GameObject offerPanel;

        private void Awake()
        {
            _instance = this;
            traiderContainer = new Dictionary<GameObject, Traider>();
        }
    }
}
