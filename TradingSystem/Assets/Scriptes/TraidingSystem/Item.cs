﻿using UnityEngine;

namespace TradingSystem
{
    [System.Serializable]
    public class Item
    {
        public string itemName;
        public int id;
        public Sprite itemImage;
        public int cost;
        public ItemType itemType;
    }

    public enum ItemType
    {
        Weapon,
        Armor,
        Consumables
    }
}
