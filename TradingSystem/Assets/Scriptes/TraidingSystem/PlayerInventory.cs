﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TradingSystem
{
    public class PlayerInventory : MonoBehaviour
    {
        [SerializeField] private List<int> _itemsId = new List<int>();
        [SerializeField] private ItemBase _itemBase;
        [SerializeField] private List<Item> _playerInventory = new List<Item>();
        [SerializeField] private ItemSlot _itemSlotPrefab;
        [SerializeField] private Transform _playerSpawnPanel;
        [SerializeField] private Text _itemText;
        [SerializeField] private GameObject _inventoryPanel;
        [SerializeField] private GameObject _tradeButton;
        [SerializeField] private int _slotsCount;
        [HideInInspector] public Traider traider;
        private ItemSlot[] _itemSlots;
        private bool _isTrading = false;

        private void Start()
        {
            _itemSlots = new ItemSlot[_slotsCount];

            for (int i = 0; i < _slotsCount; i++)
            {
                _itemSlots[i] = Instantiate(_itemSlotPrefab, _playerSpawnPanel);
            }

            for (int i = 0; i < _itemsId.Count; i++)
                _playerInventory.Add(_itemBase.TakeItemFromBase(_itemsId[i]));

            UpdatePlayerInventory();
        }

        public void UpdatePlayerInventory()
        {
            if (_itemSlots != null)
            {
                foreach (ItemSlot itemSlot in _itemSlots)
                    itemSlot.ActiveItemSlot(null);

                for (int i = 0; i < _itemsId.Count; i++)
                {
                    if (i < _itemSlots.Length)
                    {
                        if (traider != null)
                        {
                            if (traider.traiderType == _playerInventory[i].itemType)
                                _itemSlots[i].ActiveItemSlot(_playerInventory[i]);
                            else
                                _itemSlots[i].ActiveItemSlot(_playerInventory[i], false);
                        }
                        else
                            _itemSlots[i].ActiveItemSlot(_playerInventory[i]);
                    }
                }
            }
        }

        public void AddItemToInventory(int id)
        {
            _playerInventory.Add(_itemBase.TakeItemFromBase(id));
            _itemsId.Add(id);
        }

        public void RemoveItemFromInventory(Item item)
        {
            _itemBase.RemoveItemFromList(_playerInventory, item);
            _itemsId.Remove(item.id);
        }

        public void ViewItemDescription(ItemSlot itemSlot)
        {
            _itemText.text = "Name: " + itemSlot.item.itemName + ", cost: " + itemSlot.item.cost;
        }

        public void OpenInventory()
        {
            if (_inventoryPanel.activeSelf)
                _inventoryPanel.SetActive(false);
            else
            {
                UpdatePlayerInventory();
                _inventoryPanel.SetActive(true);
            }
        }

        public void StartTrade()
        {
            if (!_isTrading)
            {
                if (!_inventoryPanel.activeSelf)
                {
                    _inventoryPanel.SetActive(true);
                    UpdatePlayerInventory();
                }
                traider.traiderInventoryPanel.SetActive(true);
                TradingManager.Instance.offerPanel.SetActive(true);
                _isTrading = true;
            }
            else
            {
                if (_inventoryPanel.activeSelf)
                    _inventoryPanel.SetActive(false);
                traider.traiderInventoryPanel.SetActive(false);
                TradingManager.Instance.offerPanel.SetActive(false);
                _isTrading = false;
            }
        }

        public void TraiderNear(bool isTraiderNear, Collider other = null)
        {
            if (isTraiderNear)
            {
                if (TradingManager.Instance.traiderContainer.ContainsKey(other.gameObject))
                {
                    traider = TradingManager.Instance.traiderContainer[other.gameObject];
                    _tradeButton.SetActive(true);
                }
            }
            else
            {
                traider = null;
                _tradeButton.SetActive(false);
            }
        }
    }
}
