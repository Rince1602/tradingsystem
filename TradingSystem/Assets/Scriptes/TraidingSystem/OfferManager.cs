﻿using UnityEngine;
using CnControls;
using UnityEngine.UI;

namespace TradingSystem
{
    public class OfferManager : MonoBehaviour
    {
        [SerializeField] private Transform _playerOfferPanel;
        [SerializeField] private Transform _traiderOfferPanel;
        [SerializeField] private Text _playerOfferText;
        [SerializeField] private Text _traiderOfferText;
        private ItemSlot[] _playerItemSlots;
        private ItemSlot[] _traiderItemSlots;
        [SerializeField] private ItemSlot _itemSlotPrefab;
        [SerializeField] private int _itemCount;
        private int _playerItemCost;
        private int _traiderItemCost;

        void Start()
        {
            _playerItemSlots = new ItemSlot[_itemCount];
            _traiderItemSlots = new ItemSlot[_itemCount];
            _playerOfferText.text = "Your offer: 0";
            _traiderOfferText.text = "Traider offer: 0";

            for (int i = 0; i < _itemCount; i++)
            {
                _traiderItemSlots[i] = Instantiate(_itemSlotPrefab, _traiderOfferPanel);
                _traiderItemSlots[i].ActiveItemSlot(null);
                _playerItemSlots[i] = Instantiate(_itemSlotPrefab, _playerOfferPanel);
                _playerItemSlots[i].ActiveItemSlot(null);
            }
        }

        private void Update()
        {
            if (CnInputManager.GetButtonDown("Trade"))
            {
                Trade();
            }
        }

        private void Trade()
        {
            int tempCost = 0;
            tempCost = _playerItemCost - _traiderItemCost;
            if (TradingManager.Instance.money.MoneyValue >= Mathf.Abs(tempCost))
            {
                TradingManager.Instance.money.UpdateMoney(tempCost);

                for (int i = 0; i < _itemCount; i++)
                {
                    if (_playerItemSlots[i].item != null)
                    {
                        TradingManager.Instance.playerInventory.traider.AddItem(_playerItemSlots[i].item.id);
                        TradingManager.Instance.playerInventory.traider.UpdateTraider();
                        _playerItemSlots[i].ActiveItemSlot(null);
                    }

                    if (_traiderItemSlots[i].item != null)
                    {
                        TradingManager.Instance.playerInventory.AddItemToInventory(_traiderItemSlots[i].item.id);
                        TradingManager.Instance.playerInventory.UpdatePlayerInventory();
                        _traiderItemSlots[i].ActiveItemSlot(null);
                    }
                }

                UpdateOffer();
            }
        }

        public void UpdateOffer()
        {
            _playerItemCost = 0;
            _traiderItemCost = 0;

            for (int i = 0; i < _itemCount; i++)
            {
                if (_playerItemSlots[i].item != null)
                    _playerItemCost += _playerItemSlots[i].item.cost;
                if (_traiderItemSlots[i].item != null)
                    _traiderItemCost += _traiderItemSlots[i].item.cost;
            }

            _playerOfferText.text = "Your offer: " + _playerItemCost;
            _traiderOfferText.text = "Traider offer: " + _traiderItemCost;
        }
    }
}
