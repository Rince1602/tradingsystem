﻿using UnityEngine;
using UnityEngine.UI;

namespace TradingSystem
{
    public class Money : MonoBehaviour
    {
        [SerializeField] private int _moneyValue;
        public int MoneyValue
        {
            get { return _moneyValue; }
        }
        [SerializeField] private Text _moneyText;

        private void Start()
        {
            _moneyText.text = _moneyValue.ToString();
        }

        public void UpdateMoney(int itemCost)
        {
            _moneyValue += itemCost;
            _moneyText.text = _moneyValue.ToString();
        }
    }
}
