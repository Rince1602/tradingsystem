﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace TradingSystem
{
    public class Drop : MonoBehaviour, IDropHandler
    {
        private GameObject _childElement;

        private void Start()
        {
            _childElement = transform.GetChild(0).gameObject;
        }

        public void OnDrop(PointerEventData eventData)
        {
            ItemDragHandler drag = eventData.pointerDrag.GetComponent<ItemDragHandler>();           

            if (drag != null)
            {
                ItemSlot tempItemSlot = drag.originalParent.GetComponent<ItemSlot>();
                switch (gameObject.transform.parent.name)
                {
                    case "TraiderPanel":
                        if (!_childElement.activeSelf && drag.originalParent.parent.name == "TraiderOffer")
                        {
                            gameObject.GetComponent<ItemSlot>().ActiveItemSlot(tempItemSlot.item);
                            TradingManager.Instance.playerInventory.traider.AddItem(tempItemSlot.item.id);
                            tempItemSlot.ActiveItemSlot(null);
                            TradingManager.Instance.offerManager.UpdateOffer();
                        }
                        else
                        {
                            drag.transform.SetParent(drag.originalParent);
                        }

                        break;

                    case "PlayerPanel":
                        if (!_childElement.activeSelf && drag.originalParent.parent.name == "PlayerPanel")
                        {
                            if (TradingManager.Instance.playerInventory.traider != null)
                            {
                                if (TradingManager.Instance.playerInventory.traider.traiderType ==
                                    tempItemSlot.item.itemType)
                                    gameObject.GetComponent<ItemSlot>().ActiveItemSlot(tempItemSlot.item);
                                else
                                    gameObject.GetComponent<ItemSlot>().ActiveItemSlot(tempItemSlot.item, false);
                            }
                            else
                            {
                                gameObject.GetComponent<ItemSlot>().ActiveItemSlot(tempItemSlot.item);
                            }
                            tempItemSlot.ActiveItemSlot(null);
                        }
                        else if (!_childElement.activeSelf && drag.originalParent.parent.name == "PlayerOffer")
                        {
                            TradingManager.Instance.playerInventory.AddItemToInventory(tempItemSlot.item.id);
                            gameObject.GetComponent<ItemSlot>().ActiveItemSlot(tempItemSlot.item);
                            tempItemSlot.ActiveItemSlot(null);
                            TradingManager.Instance.offerManager.UpdateOffer();
                        }
                        else if (_childElement.activeSelf && drag.originalParent.parent.name == "PlayerPanel")
                        {
                            Item tempItem = gameObject.GetComponent<ItemSlot>().item;
                            if (TradingManager.Instance.playerInventory.traider.traiderType == tempItemSlot.item.itemType)
                                gameObject.GetComponent<ItemSlot>().ActiveItemSlot(tempItemSlot.item);
                            else
                                gameObject.GetComponent<ItemSlot>().ActiveItemSlot(tempItemSlot.item, false);
                            if (TradingManager.Instance.playerInventory.traider.traiderType == tempItem.itemType)
                                tempItemSlot.ActiveItemSlot(tempItem);
                            else
                                tempItemSlot.ActiveItemSlot(tempItem, false);
                        }
                        else
                        {
                            drag.transform.SetParent(drag.originalParent);
                        }

                        break;

                    case "TraiderOffer":
                        if (!_childElement.activeSelf && drag.originalParent.parent.name == "TraiderPanel")
                        {
                            gameObject.GetComponent<ItemSlot>().ActiveItemSlot(tempItemSlot.item);
                            TradingManager.Instance.playerInventory.traider.RemoveItem(tempItemSlot.item);
                            tempItemSlot.ActiveItemSlot(null);
                            TradingManager.Instance.offerManager.UpdateOffer();
                        }
                        else
                        {
                            drag.transform.SetParent(drag.originalParent);
                        }

                        break;

                    case "PlayerOffer":
                        if (!_childElement.activeSelf && drag.originalParent.parent.name == "PlayerPanel" &&
                            TradingManager.Instance.playerInventory.traider.traiderType ==
                            tempItemSlot.item.itemType)
                        {
                            gameObject.GetComponent<ItemSlot>().ActiveItemSlot(tempItemSlot.item);
                            TradingManager.Instance.playerInventory.RemoveItemFromInventory(tempItemSlot.item);
                            tempItemSlot.ActiveItemSlot(null);
                            TradingManager.Instance.offerManager.UpdateOffer();
                        }
                        else
                        {
                            drag.transform.SetParent(drag.originalParent);
                        }

                        break;
                }
            }
        }
    }
}
