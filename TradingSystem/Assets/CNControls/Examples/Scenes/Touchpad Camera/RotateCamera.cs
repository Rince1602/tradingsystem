﻿using CnControls;
using UnityEngine;

namespace Examples.Scenes.TouchpadCamera
{
	public class RotateCamera : MonoBehaviour
    {
        public float RotationSpeed = 15f;
        public Transform OriginTransform;

        public void Update()
        {
			float horizontalMovement = CnInputManager.GetAxis ("gHorizontal");
			float verticalMovement = CnInputManager.GetAxis ("gVertical");
			OriginTransform.Rotate (Vector3.left, verticalMovement * Time.deltaTime * RotationSpeed);
			OriginTransform.Rotate (Vector3.up, horizontalMovement * Time.deltaTime * RotationSpeed);
        }
    }
}